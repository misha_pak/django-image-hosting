# Django-Image-Hosting

A test project which goal is to create a simple RESTful API to store images

## How To

Just clone the repo and run

    docker-compose up

## Endpoints

There are couple of endpoints that allow you to utilize this application.

| Type | URI | Description |  
| :---: | :---: | :---: |  
| GET | /api/image | Lists all uploaded images |  
| GET | /api/image/<image_uuid>/ | Loads a specific image | 
| POST | /api/image_upload| Uploads a new image from url