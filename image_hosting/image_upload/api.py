from rest_framework import viewsets, permissions
from rest_framework.response import Response

from image.serializers import ImageUUIDSerializer, ImageSerializer
from image_upload.domain.services import ImageUploadService
from image_upload.serializers import UploadURLSerializer


class ImageUploadViewset(viewsets.ViewSet):
    permission_classes = [permissions.AllowAny, ]

    def create(self, request):
        upload_url_ser = UploadURLSerializer(data=request.data)
        upload_url_ser.is_valid(raise_exception=True)
        upload = upload_url_ser.validated_data

        image = ImageUploadService.create_image_from_upload(upload=upload)

        return Response(ImageSerializer(image).data)
