from django.contrib import admin
from django.urls import path, include

from image.urls import image_urlpatterns
from image_upload.urls import image_upload_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(image_urlpatterns)),
    path('api/', include(image_upload_urlpatterns)),
]
