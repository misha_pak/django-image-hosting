import uuid as _uuid
from dataclasses import dataclass


@dataclass
class Upload:
    source_url: str = None
    data: bytes = None
    mime_type: str = None
    uuid: _uuid.UUID = None
