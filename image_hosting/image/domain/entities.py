from dataclasses import dataclass
import uuid as _uuid
from django.core.files import File


@dataclass
class Image:
    file: File = None
    mime_type: str = None
    uuid: _uuid.UUID = None

