# Generated by Django 3.1.7 on 2021-10-23 13:47

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DBImage',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('mime_type', models.CharField(max_length=32)),
                ('file', models.ImageField(upload_to='static/images')),
            ],
        ),
    ]
