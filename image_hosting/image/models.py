import uuid as _uuid
from django.db import models


class DBImage(models.Model):
    uuid = models.UUIDField(primary_key=True, default=_uuid.uuid4, editable=False)
    mime_type = models.CharField(max_length=32)
    file = models.ImageField(upload_to='static/images')
